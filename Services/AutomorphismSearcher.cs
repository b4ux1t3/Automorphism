
public class AutomorphismSearcher(FileReader fr) : IHostedService
{
    private bool _go = false;
    public UInt128 CurrentNumber { get; private set; } = 5;

    private void CrunchNumbers()
    {
        while (_go)
        {
            if(_matches(CurrentNumber)){
                File.AppendAllText(FileReader.FileWriteLocation, $"{CurrentNumber},{CurrentNumber*CurrentNumber},{DateTimeOffset.UtcNow.ToUnixTimeMilliseconds()}\n");
            }

            if (_matches(CurrentNumber + 1))
            {
                var plusOne = CurrentNumber + 1;
                File.AppendAllText(FileReader.FileWriteLocation, $"{plusOne},{plusOne*plusOne},{DateTimeOffset.UtcNow.ToUnixTimeMilliseconds()}\n");
            }
            fr.CurrentNumber = CurrentNumber;

            CurrentNumber += 10;
        }
    }
    public async Task StartAsync(CancellationToken cancellationToken)
    {
        if (File.Exists(FileReader.FileWriteLocation))
        {
            var lines = File.ReadAllLines(FileReader.FileWriteLocation);
            if (lines.Length != 0) CurrentNumber = UInt128.Parse(lines[^1].Split(',')[0]) + 10;
        }
        _go = true;
        Task.Run(CrunchNumbers);
    }

    public async Task StopAsync(CancellationToken cancellationToken)
    {
        _go = false;
    }

    private static bool _matches(UInt128 n)
    {
        var nString = $"{n}";
        var sString = $"{n * n}";
        var size = nString.Length;
        var ending = sString[^size..];
        return ending == nString;
    }
}