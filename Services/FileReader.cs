using Microsoft.Extensions.DependencyInjection.Extensions;

public static class FileReaderExtensions {
    public static IServiceCollection TryAddFileReader(this IServiceCollection svcs){
        svcs.TryAddSingleton<FileReader>();

        return svcs;
    }
}

public class FileReader{
    public static string FileWriteLocation = "./findings.txt";
    public UInt128 CurrentNumber { get; set; }
    public string[] Findings => File.ReadAllLines(FileWriteLocation);
}